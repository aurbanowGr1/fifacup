package db;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAUtils {
	
	
	public static void executeTransaction(EntityManager em,Runnable rnable)
	{
		
		if(!em.getTransaction().isActive())
		{
		em.getTransaction().begin();
		
		try {
			rnable.run();
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw new RuntimeException(e);
		}
		}
		else
		{
			rnable.run();
		}
		
	}
	}