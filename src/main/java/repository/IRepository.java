package repository;

//* main interface repository *//

import java.util.List;

public interface IRepository<T> {
	public T get(int id);
	public List<T> getAll();
	public void add(T entity);
	public void delete(T entity);
	public void update(T entity);
}
