package repository;

import java.util.List;

import domain.Match;
import domain.Player;


public interface IMatchRepository extends IRepository<Match> {
	
	List<Match> getByHomeTeam(String homeTeam);
	List<Match> getByAwayTeam(String awayTeam);
	List<Match> getByTeam(String Team);
	Match getMatchById(Integer id);
	
}
