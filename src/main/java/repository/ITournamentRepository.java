package repository;
import java.util.List;

import domain.Match;
import domain.Tournament;
import domain.enums.League;
import domain.enums.Platform;
import domain.enums.Status;

public interface ITournamentRepository extends IRepository<Tournament> {

	List<Tournament> getByLeague(League league);
	List<Tournament> getByStatus(Status status);
	List<Tournament> getByPlatform(Platform platform);
	Tournament getTournamentById(Integer id);
}
