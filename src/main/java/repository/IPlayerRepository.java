package repository;

import java.util.Collection;
import java.util.List;

import domain.Player;
import domain.enums.League;

public interface IPlayerRepository extends IRepository<Player> {

	List<Player> getByIds(Collection<Integer> ids);
	Player getById(Integer id);
	List<Player> getByName(String name);
	List<Player> getByTeamName(String teamName);
	List<Player> getByLeague(League league);
}
