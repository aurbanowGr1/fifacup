package repository.impl;

import java.util.List;

import javax.persistence.EntityManager;

import repository.IRepository;
import db.JPAUtils;

public class Repository<T> implements IRepository<T> {
	
	protected final EntityManager em;
	protected final Class<T> clazz;
	
	public Repository(Class<T> clazz,EntityManager em) {
		this.clazz = clazz;
		this.em = em;
	}
	
		
	public T get(int id) {
		return em.find(clazz, id);
	}
	
	public List<T> getAll() {
		return em.createQuery("Select t From " + clazz.getCanonicalName() + " t", clazz).getResultList();
	}
	
	public void add(final T entity) {

		JPAUtils.executeTransaction(em,new Runnable() {
			
			
			public void run() {
				em.persist(entity);
				
			}
		});
	}
	
	public void delete(final T entity) {

		JPAUtils.executeTransaction(em, new Runnable() {
			
			public void run() {
				
				if(em.contains(entity))
				{
				em.remove(entity);
				}
				else
				{
				em.remove(em.merge(entity));
				}
				
			}
		});
		
	} 
	
	public void update(final T entity) {
		JPAUtils.executeTransaction(em, new Runnable() {
			
			public void run() {
				
				em.merge(entity);
				
			}
		});
	}
	
}

