package repository.impl;

import java.util.List;

import javax.persistence.EntityManager;

import repository.ITournamentRepository;
import domain.Match;
import domain.Tournament;
import domain.enums.League;
import domain.enums.Platform;
import domain.enums.Status;

public class TournamentRepository extends Repository<Tournament> implements ITournamentRepository  {
	

	

	public TournamentRepository(EntityManager em) {
		super(Tournament.class,em);
	}
	


	public List<Tournament> getByLeague(League league) {
		
		return em.createQuery("Select l From Tournament l Where l.League = :league", clazz).setParameter("League", league).getResultList();
	}

	public List<Tournament> getByStatus(Status status) {
		
		return em.createQuery("Select s From Tournament s Where s.Status = :status", clazz).setParameter("Status", status).getResultList();
		
	}

	public List<Tournament> getByPlatform(Platform platform) {
		
		return em.createQuery("Select p From Tournament p Where p.Platform = :platform", clazz).setParameter("Platform", platform).getResultList();
	}



	@Override
	public Tournament getTournamentById(Integer id) {
		
		return (Tournament) em.createQuery("Select t From Tournament t Where t.id = :id", clazz).setParameter("id", id).getSingleResult();

		
	}
	
}
