 package repository.impl;

import java.util.List;

import javax.persistence.EntityManager;

import repository.IMatchRepository;
import domain.Match;
import domain.Player;

public class MatchRepository extends Repository<Match> implements IMatchRepository  {
	


	public MatchRepository(EntityManager em) {
		super(Match.class,em);
	}
	
	public List<Match> getByHomeTeam(String homeTeam) {
		
		return em.createQuery("Select t From Match t Where t.homeTeam = :homeTeam", clazz).setParameter("homeTeam", homeTeam).getResultList();
	}

	public List<Match> getByAwayTeam(String awayTeam) {

		return em.createQuery("Select t From Match t Where t.awayTeam = :awayTeam", clazz).setParameter("awayTeam", awayTeam).getResultList();
	}

	public List<Match> getByTeam(String team) {
		
		return em.createQuery("Select t From Match t Where t.awayTeam = :team or t.homeTeam = :team", clazz).setParameter("team", team).getResultList();

	}

	@Override
	public Match getMatchById(Integer id) {

		return (Match) em.createQuery("Select i From Match i Where i.id = :id", clazz).setParameter("id", id).getSingleResult();
	}
	
}
