package repository.impl;

import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;

import repository.IPlayerRepository;
import domain.Player;
import domain.enums.League;

public class PlayerRepository extends Repository<Player> implements IPlayerRepository  {
	

	
	public PlayerRepository(EntityManager em) {
		super(Player.class,em);
		
	}
	
	
	

	public List<Player> getByName(String name) {
		
		return em.createQuery("Select n From Player n Where n.name = :name", clazz).setParameter("name", name).getResultList();
		
		
	}

	public List<Player> getByTeamName(String teamName) {
		return em.createQuery("Select tm From Player tm Where tm.teamName = :teamName", clazz).setParameter("teamName", teamName).getResultList();

	}

	public List<Player> getByLeague(League league) {
		
		return em.createQuery("Select l From Player l Where l.league = :league", clazz).setParameter("league", league).getResultList();
	}




	@Override
	public List<Player> getByIds(Collection<Integer> ids) {

		return em.createQuery("Select tm From Player tm Where tm.id IN :ids", clazz).setParameter("ids", ids).getResultList();
	}




	@Override
	public Player getById(Integer id) {

		return (Player) em.createQuery("Select i From Player i Where i.id = :id", clazz).setParameter("id", id).getSingleResult();
	}





	
}
