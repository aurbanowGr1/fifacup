package fifacup.config;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.hibernate.cfg.Environment;
import org.hibernate.cfg.ImprovedNamingStrategy;
import org.hibernate.dialect.MySQLDialect;
import org.hibernate.jpa.AvailableSettings;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.instrument.classloading.InstrumentationLoadTimeWeaver;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

@Configuration
@EnableJpaRepositories(basePackages = "fifacup.domain.repository")
public class JpaConfiguration {
	
	@Bean
	public DataSource dataSource() {
		MysqlDataSource datasource = new MysqlDataSource();
		datasource.setUrl("jdbc:mysql://localhost/fifacup");
		datasource.setUser("root");
		datasource.setPassword("");
		return datasource;
		// return new EmbeddedDatabaseBuilder().setType(EmbeddedDatabaseType.H2).build();
	}
	
	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() throws Exception {
		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		vendorAdapter.setGenerateDdl(true);
		vendorAdapter.setShowSql(true);
		
		LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
		factory.setPersistenceUnitName("fifacup");
		factory.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
		factory.setJpaVendorAdapter(vendorAdapter);
		factory.setPackagesToScan("domain");
		factory.setDataSource(dataSource());
		
		factory.setJpaProperties(jpaProperties());
		factory.setLoadTimeWeaver(new InstrumentationLoadTimeWeaver());
		
		return factory;
	}
	
	private Properties jpaProperties() {
		Properties props = new Properties();
		props.put(AvailableSettings.NAMING_STRATEGY, ImprovedNamingStrategy.class.getName());
//		props.put(Environment.HBM2DDL_AUTO, "create-drop");
		props.put(Environment.HBM2DDL_AUTO, "update");
		props.put(Environment.DIALECT, MySQLDialect.class.getName());
		props.put(Environment.GLOBALLY_QUOTED_IDENTIFIERS, "true");
		return props;
	}

	
}
