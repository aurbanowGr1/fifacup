package fifacup.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "fifacup.config")
public class ApplicationConfiguration {

	
}
