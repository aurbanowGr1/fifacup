package fifacup.controller;

import java.util.EnumSet;
import java.util.List;
import java.util.Random;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.YamlProcessor.MatchStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import repository.IMatchRepository;
import repository.IPlayerRepository;
import repository.ITournamentRepository;
import repository.impl.MatchRepository;
import repository.impl.PlayerRepository;
import repository.impl.TournamentRepository;
import db.JPAUtils;
import domain.Match;
import domain.Player;
import domain.Tournament;
import domain.enums.League;
import domain.enums.Platform;
import domain.enums.Status;
import form.MatchForm;
import form.PlayerForm;
import form.TournamentForm;

@Controller
public class TournamentController {
	
	@Autowired
	private EntityManagerFactory emf;
	@RequestMapping("/")
	public ModelAndView index(ModelAndView view) {
		
		ITournamentRepository tournamentRepository = new TournamentRepository(emf.createEntityManager());
		
		
		
		view.setViewName("index");
		view.addObject("tournaments", tournamentRepository.getAll());
		return view;
	}

	@RequestMapping("/createTournament")
	public ModelAndView newTournament(ModelAndView view)
	{
		IPlayerRepository playerRepository = new PlayerRepository(emf.createEntityManager());
		view.setViewName("createTournament");
		view.addObject("players", playerRepository.getAll());
		view.addObject("leagues", EnumSet.allOf(League.class));
		view.addObject("platforms", EnumSet.allOf(Platform.class));
		view.addObject("statuss", EnumSet.allOf(Status.class));
		return view;
	}
	
	@RequestMapping(value="/createTournament",method=RequestMethod.POST )
	public String handleNewTournament(@Valid final TournamentForm form,BindingResult result,Model model)
	{
		EntityManager em = emf.createEntityManager();
		
		final IPlayerRepository playerRepository = new PlayerRepository(em);
		if (result.hasErrors()) {
			model.addAttribute("players", playerRepository.getAll());
			model.addAttribute("result", result);
			model.addAttribute("leagues", EnumSet.allOf(League.class));
			model.addAttribute("platforms", EnumSet.allOf(Platform.class));
			model.addAttribute("statuss", EnumSet.allOf(Status.class));
			
            return "createTournament";
        }
		final ITournamentRepository tournamentRepository = new TournamentRepository(em);

		JPAUtils.executeTransaction(em, new Runnable() {
			
			@Override
			public void run() {
				Tournament t = new Tournament();
				List<Player> players = playerRepository.getByIds(form.getPlayers());
				Player winner = (Player) playerRepository.getById(form.getWinner());
				t.getParticipants().addAll(players);
				t.setName(form.getName());
				t.setLeague(form.getLeague());
				t.setStatus(form.getStatus());
				t.setPlatform(form.getPlatform());
				t.setMaxNumberOfParticipants(form.getNop());
				t.setWinner(winner);
				
				tournamentRepository.add(t);
			}
		});

        
        return "redirect:";
        
	}
	
	@RequestMapping("/createPlayer")
	public ModelAndView newPlayer(ModelAndView view)
	{
		view.setViewName("createPlayer");
		view.addObject("leagues", EnumSet.allOf(League.class));
		return view;
	}
	
	
	
	@RequestMapping(value="/createPlayer",method=RequestMethod.POST )
	public String handleNewPlayer(@Valid final PlayerForm form,BindingResult result,Model model)
	{
		EntityManager em = emf.createEntityManager();
		
		final IPlayerRepository playerRepository = new PlayerRepository(em);
		if (result.hasErrors()) {
			model.addAttribute("leagues", EnumSet.allOf(League.class));
			model.addAttribute("players", playerRepository.getAll());
			model.addAttribute("result", result);
            return "createPlayer";
        }


		JPAUtils.executeTransaction(em, new Runnable() {
			
			@Override
			public void run() {
			    Player p = new Player();
				p.setAge(form.getAge());
				p.setName(form.getName());
				p.setNick(form.getNick());
				p.setPassword(form.getPassword());
				p.setSurname(form.getSurname());
				p.setTeamName(form.getTeamName());
				p.setLeague(form.getLeague());
				playerRepository.add(p);
			}
		});

        
        return "redirect:";
        
	}
	
	
	@RequestMapping("/createMatch")
	public ModelAndView newMatch(ModelAndView view)
	{
		IPlayerRepository playerRepository = new PlayerRepository(emf.createEntityManager());
		
		view.setViewName("createMatch");
		view.addObject("players", playerRepository.getAll());
		return view;
	}
	
	
	@RequestMapping(value="/createMatch",method=RequestMethod.POST )
	public String handleNewMatch(@Valid final MatchForm form,BindingResult result,Model model)
	{
		EntityManager em = emf.createEntityManager();
		
		final IMatchRepository matchRepository = new MatchRepository(em);
		final IPlayerRepository playerRepository = new PlayerRepository(em);
		
		
		
		if (result.hasErrors()) {
			model.addAttribute("players", playerRepository.getAll());
			model.addAttribute("result", result);
            return "createMatch";
        }


		JPAUtils.executeTransaction(em, new Runnable() {
			
			@Override
			public void run() {
			    Match m = new Match();
			    Player homeTeam = playerRepository.getById(form.getHomeTeam());
			    Player awayTeam = playerRepository.getById(form.getAwayTeam());
			    
			    m.setHomeTeamGoals(form.getHtg());
			    m.setAwayTeamGoals(form.getAtg());
			    m.setHomeTeam(homeTeam);
			    m.setAwayTeam(awayTeam);
			    matchRepository.add(m);
			    
			}
		});

        
        return "redirect:";
        
	}
	
	
	
	
	
	
	
	@RequestMapping("/showMatch")
	public ModelAndView showMatch(ModelAndView view) {
		
		IMatchRepository matchRepository = new MatchRepository(emf.createEntityManager());
		
		
		
		view.setViewName("showMatch");
		view.addObject("matches", matchRepository.getAll());
		return view;
	}
	


@RequestMapping("/showPlayer")
public ModelAndView showPlayer(ModelAndView view) {
	
	IPlayerRepository playerRepository = new PlayerRepository(emf.createEntityManager());
	
	
	
	view.setViewName("showPlayer");
	view.addObject("players", playerRepository.getAll());
	return view;
}
	
	
	






@RequestMapping("/deleteMatch")
public ModelAndView deleteMatch(ModelAndView view)
{
			view.setViewName("deleteMatch");
	return view;
}


@RequestMapping(value="/deleteMatch",method=RequestMethod.POST )
public String handledeleteMatch(@Valid final MatchForm form,BindingResult result,Model model)
{
	EntityManager em = emf.createEntityManager();
	
	final IMatchRepository matchRepository = new MatchRepository(em);
	
	if (result.hasErrors()) {
		model.addAttribute("result", result);
        return "deleteMatch";
    }


	JPAUtils.executeTransaction(em, new Runnable() {
		
		@Override
		public void run() {

			matchRepository.delete(matchRepository.getMatchById(form.getMatch()));

		    
		}
	});

    
    return "redirect:";
    
}




@RequestMapping("/deletePlayer")
public ModelAndView deletePlayer(ModelAndView view)
{
			view.setViewName("deletePlayer");
	return view;
}


@RequestMapping(value="/deletePlayer",method=RequestMethod.POST )
public String handledeletePlayer(@Valid final PlayerForm form,BindingResult result,Model model)
{
	EntityManager em = emf.createEntityManager();
	
	final IPlayerRepository playerRepository = new PlayerRepository(em);
	
	if (result.hasErrors()) {
		model.addAttribute("result", result);
        return "deletePlayer";
    }


	JPAUtils.executeTransaction(em, new Runnable() {
		
		@Override
		public void run() {

			playerRepository.delete(playerRepository.getById(form.getPlayer()));

		    
		}
	});

    
    return "redirect:";
    
}
	




@RequestMapping("/deleteTournament")
public ModelAndView deleteTournament(ModelAndView view)
{
			view.setViewName("deleteTournament");
	return view;
}


@RequestMapping(value="/deleteTournament",method=RequestMethod.POST )
public String handledeleteTournament(@Valid final TournamentForm form,BindingResult result,Model model)
{
	EntityManager em = emf.createEntityManager();
	
	final ITournamentRepository tournamentRepository = new TournamentRepository(em);
	
	if (result.hasErrors()) {
		model.addAttribute("result", result);
        return "deleteTournament";
    }


	JPAUtils.executeTransaction(em, new Runnable() {
		
		@Override
		public void run() {

			tournamentRepository.delete(tournamentRepository.getTournamentById(form.getTournament()));

		    
		}
	});

    
    return "redirect:";
    
}


}




