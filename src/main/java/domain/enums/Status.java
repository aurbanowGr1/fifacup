package domain.enums;

public enum Status
{

		Entry,
		Started,
		Finished;

}
