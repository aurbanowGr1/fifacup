package domain;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Future;

import domain.enums.*;

@Entity
public class Tournament extends AbstractEntity
{
	@Basic(optional=false)
	private String name;
	
	@Basic(optional=false)
	@Enumerated(EnumType.STRING)
	private League league;
	
	@ManyToMany(cascade={CascadeType.PERSIST, CascadeType.MERGE})
	private List<Player> participants = new ArrayList<Player>();
	
	@Basic(optional=false)	
	private int maxNumberOfParticipants;
	
	@Basic(optional=false)
	@Temporal(TemporalType.DATE)
	@Future
	private Date date;
	
	@Basic(optional=false)
	private Platform platform;
	
	@Basic(optional=false)
	private Status status;
	
	@ManyToOne(optional=false, cascade={CascadeType.PERSIST, CascadeType.MERGE})
	private Player winner;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public League getLeague() {
		return league;
	}
	public void setLeague(League league) {
		this.league = league;
	}
	public List<Player> getParticipants() {
		return participants;
	}
	public int getMaxNumberOfParticipants() {
		return maxNumberOfParticipants;
	}
	public void setMaxNumberOfParticipants(int maxNumberOfParticipants) {
		this.maxNumberOfParticipants = maxNumberOfParticipants;
	}
	public void setParticipants(List<Player> participants) {
		this.participants = participants;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Platform getPlatform() {
		return platform;
	}
	public void setPlatform(Platform platform) {
		this.platform = platform;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public Player getWinner() {
		return winner;
	}
	public void setWinner(Player winner) {
		this.winner = winner;
	}
	
}
