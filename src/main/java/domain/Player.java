package domain;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Min;

import domain.enums.*;

@Entity
public class Player extends AbstractEntity
{
	@Basic(optional=false)
	private String nick;
	
	@Basic(optional=false)
	private String password;
	
	@Basic(optional=false)
	private String name;
	
	@Basic(optional=false)
	private String surname;
	
	@Min(0)
	@Basic(optional=false)
	private int age;
	
	@Basic(optional=false)
	private String teamName;
	
	@Basic(optional=false)
	@Enumerated(EnumType.STRING)
	private League league;
	
	@Basic(optional=false)
	private int wins;
	
	@Basic(optional=false)
	private int draws;
	
	@Basic(optional=false)
	private int loses;
	
	@Basic(optional=false)
	private int points;
	
	@Basic(optional=false)
	private int goalsFor;
	
	@Basic(optional=false)
	private int goalsAgainst;


	public String getNick() {
		return nick;
	}
	public void setNick(String nick) {
		this.nick = nick;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getTeamName() {
		return teamName;
	}
	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}
	public League getLeague() {
		return league;
	}
	public void setLeague(League league) {
		this.league = league;
	}
	public int getWins() {
		return wins;
	}
	public void setWins(int wins) {
		this.wins = wins;
	}
	public int getDraws() {
		return draws;
	}
	public void setDraws(int draws) {
		this.draws = draws;
	}
	public int getLoses() {
		return loses;
	}
	public void setLoses(int loses) {
		this.loses = loses;
	}
	public int getPoints() {
		return points;
	}
	public void setPoints(int points) {
		this.points = points;
	}
	public int getGoalsFor() {
		return goalsFor;
	}
	public void setGoalsFor(int goalsFor) {
		this.goalsFor = goalsFor;
	}
	public int getGoalsAgainst() {
		return goalsAgainst;
	}
	public void setGoalsAgainst(int goalsAgainst) {
		this.goalsAgainst = goalsAgainst;
	}
	
}

	
	
	
	

