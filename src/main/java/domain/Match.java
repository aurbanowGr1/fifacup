package domain;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Match extends AbstractEntity
{
	@ManyToOne(optional=false, cascade={CascadeType.PERSIST, CascadeType.MERGE})
	private Player homeTeam;
	
	@ManyToOne(optional=false, cascade={CascadeType.PERSIST, CascadeType.MERGE})
	private Player awayTeam;
	
	@Basic(optional=false)
	private int homeTeamGoals;
	
	@Basic(optional=false)
	private int awayTeamGoals;
	
	@Basic(optional=false)
	@Temporal(TemporalType.DATE)
	private Date date;
	
	public Player getHomeTeam() {
		return homeTeam;
	}
	public void setHomeTeam(Player homeTeam) {
		this.homeTeam = homeTeam;
	}
	public Player getAwayTeam() {
		return awayTeam;
	}
	public void setAwayTeam(Player awayTeam) {
		this.awayTeam = awayTeam;
	}
	public int getHomeTeamGoals() {
		return homeTeamGoals;
	}
	public void setHomeTeamGoals(int homeTeamGoals) {
		this.homeTeamGoals = homeTeamGoals;
	}
	public int getAwayTeamGoals() {
		return awayTeamGoals;
	}
	public void setAwayTeamGoals(int awayTeamGoals) {
		this.awayTeamGoals = awayTeamGoals;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}


}
