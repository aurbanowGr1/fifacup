package form;

import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import domain.Player;

public class MatchForm {



	public Integer getHomeTeam() {
		return homeTeam;
	}

	public void setHomeTeam(Integer homeTeam) {
		this.homeTeam = homeTeam;
	}

	public Integer getAwayTeam() {
		return awayTeam;
	}

	public void setAwayTeam(Integer awayTeam) {
		this.awayTeam = awayTeam;
	}

	public Integer getHtg() {
		return htg;
	}

	public void setHtg(Integer htg) {
		this.htg = htg;
	}

	public Integer getAtg() {
		return atg;
	}

	public void setAtg(Integer atg) {
		this.atg = atg;
	}

	public Integer getMatch() {
		return match;
	}

	public void setMatch(Integer match) {
		match = match;
	}

	private Integer homeTeam;

	@Min(value=0)
	@NotNull(message="Musisz podac id meczu.")
	private Integer match;
	

	private Integer awayTeam;
	
	@Min(value=0)
	@NotNull(message="Musisz podac liczbe goli.")
	private Integer htg;
	
	@Min(value=0)
	@NotNull(message="Musisz podac liczbe goli.")
	private Integer atg;

}
