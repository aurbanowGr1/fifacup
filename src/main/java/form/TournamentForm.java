package form;

import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import domain.Player;
import domain.enums.League;
import domain.enums.Platform;
import domain.enums.Status;

public class TournamentForm {

@NotEmpty(message="Musisz podac conajmniej jednego gracza.")
private Set<Integer> players = new HashSet<Integer>();
@NotBlank(message="Musisz podac nazwe turnieju")
private String name;


private Integer winner;

private League league;
private Status status;
private Platform platform;

@Min(value=0)
@NotNull(message="Musisz podaj liczbe.")
private Integer nop;
@Min(value=0)
@NotNull(message="Musisz podac id.")
private Integer tournament;

public Integer getTournament() {
	return tournament;
}

public void setTournament(Integer tournament) {
	this.tournament = tournament;
}

public Integer getNop() {
	return nop;
}

public void setNop(Integer nop) {
	this.nop = nop;
}

public Set<Integer> getPlayers() {
	return players;
}

public void setPlayers(Set<Integer> players) {
	this.players = players;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public League getLeague() {
	return league;
}

public void setLeague(League league) {
	this.league = league;
}

public Status getStatus() {
	return status;
}

public void setStatus(Status status) {
	this.status = status;
}

public Platform getPlatform() {
	return platform;
}

public void setPlatform(Platform platform) {
	this.platform = platform;
}

public Integer getWinner() {
	return winner;
}

public void setWinner(Integer winner) {
	this.winner = winner;
}


	
}
