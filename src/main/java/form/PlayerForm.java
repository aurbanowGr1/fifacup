package form;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import domain.enums.League;

public class PlayerForm {
	
	@NotBlank(message="Musisz podac nick.")
	private String nick;

	@NotBlank(message="Musisz podac haslo.")
	private String password;
	
	@NotBlank(message="Musisz podac imie.")
	private String name;
	
	@NotBlank(message="Musisz podac nazwisko.")
	private String surname;
	
	@Min(value=0)
	@NotNull(message="Musisz podac wiek.")
	private Integer age;
	
	@NotBlank(message="Musisz podac nazwe druzyny.")
	private String teamName;
	@Min(value=0)
	@NotNull(message="Musisz podac id.")
	private Integer player;
	
	public Integer getPlayer() {
		return player;
	}


	public void setPlayer(Integer player) {
		this.player = player;
	}


	private League league;
	

	private int wins;
	

	private int draws;
	

	private int loses;
	

	private int points;
	

	private int goalsFor;
	

	private int goalsAgainst;
	

	public String getNick() {
		return nick;
	}


	public void setNick(String nick) {
		this.nick = nick;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getSurname() {
		return surname;
	}


	public void setSurname(String surname) {
		this.surname = surname;
	}


	public Integer getAge() {
		return age;
	}


	public void setAge(Integer age) {
		this.age = age;
	}


	public String getTeamName() {
		return teamName;
	}


	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}


	public League getLeague() {
		return league;
	}


	public void setLeague(League league) {
		this.league = league;
	}


	public int getWins() {
		return wins;
	}


	public void setWins(int wins) {
		this.wins = wins;
	}


	public int getDraws() {
		return draws;
	}


	public void setDraws(int draws) {
		this.draws = draws;
	}


	public int getLoses() {
		return loses;
	}


	public void setLoses(int loses) {
		this.loses = loses;
	}


	public int getPoints() {
		return points;
	}


	public void setPoints(int points) {
		this.points = points;
	}


	public int getGoalsFor() {
		return goalsFor;
	}


	public void setGoalsFor(int goalsFor) {
		this.goalsFor = goalsFor;
	}


	public int getGoalsAgainst() {
		return goalsAgainst;
	}


	public void setGoalsAgainst(int goalsAgainst) {
		this.goalsAgainst = goalsAgainst;
	}


	
}
