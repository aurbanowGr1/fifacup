<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML>
<html>
<body>

 <form action="showMatch" method="post">
	<c:if test="${not empty players}">
		<ul>
		<table border="1" style="width:100%">
  <tr>
   <td>ID</td>
   <td>nick</td>
   <td>password</td>
   <td>name</td>
   <td>surname</td>
   <td>age</td>
   <td>teamName</td>
   <td>league</td>
   </tr>


			<c:forEach var="player" items="${players}"><tr>
				<td>${player.id}</td> <td>${player.nick}</td>
				<td>${player.password}</td> <td>${player.name}</td> <td>${player.surname}</td>
				<td>${player.age}</td> <td>${player.teamName}</td>
				<td>${player.league}</td>
				</tr>
			</c:forEach>
			</table>
		</ul>
	</c:if>
	
</form>
</body>
</html>
