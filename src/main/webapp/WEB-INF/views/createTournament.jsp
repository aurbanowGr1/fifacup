<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML>
<html>
<body>
 <form action="createTournament" method="post">
 
 	<c:if test="${result != null}">
	${result.getFieldError("name").getDefaultMessage()}
	</c:if>
		name: <input type="text" name="name"><br><br>
		
		 	<c:if test="${result != null}">
	${result.getFieldError("nop").getDefaultMessage()}
	</c:if>
		Maximum number of participants <input type="text" name="nop"><br><br>
 
<label>participants:</label>
	<c:if test="${result != null}">
	${result.getFieldError("players").getDefaultMessage()}
	</c:if>
  		<select name="players" multiple="multiple">
			<c:forEach var="player" items="${players}">
				<option value="${player.id}">${player.name} ${player.surname} </option>
			</c:forEach>
		</select>
		<br><br><br>
		

		
		<label>league</label><br>
		    		<select name="league">
			<c:forEach var="league" items="${leagues}">
				<option value="${league.name()}">${league.name()} </option>
			</c:forEach>
			</select><br>
			

		<label>platform</label><br>
				<select name="platform">
			<c:forEach var="platform" items="${platforms}">
				<option value="${platform.name()}">${platform.name()} </option>
			</c:forEach>
			</select>
			<br>
			
			<label>status</label><br>			
				<select name="status">
			<c:forEach var="status" items="${statuss}">
				<option value="${status.name()}">${status.name()} </option>
			</c:forEach>
			
					</select><br>
		
		
		
		
		<label>winner:</label>
	<c:if test="${result != null}">
	${result.getFieldError("winner").getDefaultMessage()}
	</c:if>
  		<select name="winner" >
			<c:forEach var="player" items="${players}">
				<option value="${player.id}">${player.name} ${player.surname} </option>
			</c:forEach>
		</select>
		<br><br><br>
		
		
		
		
  <input type="submit" value="Add">
  

</form> 

</body>
</html>
