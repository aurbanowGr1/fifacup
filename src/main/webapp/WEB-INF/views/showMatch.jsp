<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML>
<html>
<body>

 <form action="showMatch" method="post">
 
	<c:if test="${not empty matches}">
		<ul>
		<table border="1" style="width:100%">
  <tr>
   <td>ID</td>
   <td>HomeTeam</td>
   <td>AwayTeam</td>
   <td>HomeTeamGoals</td>
   <td>AwayTeamGoals</td>

   </tr>


			<c:forEach var="match" items="${matches}"><tr>
				<td>${match.id}</td> <td>${match.homeTeam.teamName}</td>
				<td>${match.awayTeam.teamName}</td> 
				
				<td>${match.homeTeamGoals}</td> 
				
				<td>${match.awayTeamGoals}</td>

				
				</tr>
			</c:forEach>
			</table>
		</ul>
	</c:if>

</form>
</body>
</html>
