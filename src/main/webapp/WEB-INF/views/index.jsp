<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML>
<html>
   <body>
	<center><h2>Welcome to FifaCup!</h2></center><br><br>
	<center>||<a href="createTournament">Create Tournament</a>||
	<a href="createPlayer">Create Player</a>||
	<a href="createMatch">Add Match</a>||<br>
	<a href="deletePlayer">Delete Player</a>||
	<a href="deleteMatch">Delete Match</a>||
	<a href="deleteTournament">Delete Tournament</a>||<br>
	<a href="showPlayer">Show Players</a>||
	<a href="showMatch">Show Matches</a>||</center><br>
	<c:if test="${not empty tournaments}">
		<ul>
		<table border="1" style="width:100%">
  <tr>
   <td>ID</td>
   <td>League</td>
   <td>M.n.o.p</td>
   <td>Name</td>
   <td>Platform</td>
   <td>Status</td>
   <td>Winner</td>
   <td>Participants</td>
   </tr>


			<c:forEach var="tournament" items="${tournaments}"><tr>
				<td>${tournament.id}</td> <td>${tournament.league}</td>
				<td>${tournament.maxNumberOfParticipants}</td> <td>${tournament.name}</td> <td>${tournament.platform}</td>
				<td>${tournament.status}</td> <td>${tournament.winner.nick}</td>
				<td><c:forEach var="participant" items="${tournament.participants}">${participant.name} ${participant.surname}<br></c:forEach></td>
				</tr>
			</c:forEach>
			</table>
		</ul>
	</c:if>

</body>
</html>
