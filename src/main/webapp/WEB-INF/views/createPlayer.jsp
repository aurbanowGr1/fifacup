<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML>
<html>
<body>
 <form action="createPlayer" method="post">
<label>nick:</label><br>		
	<c:if test="${result != null}">
	${result.getFieldError("nick").getDefaultMessage()}
	</c:if>
		 <input type="text" name="nick"><br>

	<label>password:</label><br>		
	<c:if test="${result != null}">
	${result.getFieldError("password").getDefaultMessage()}
	</c:if>
		<input type="password" name="password"><br>
		
		<label>name:</label><br>		
	<c:if test="${result != null}">
	${result.getFieldError("name").getDefaultMessage()}
	</c:if>
		<input type="text" name="name"><br>
		
		<label>surname:</label><br>		
	<c:if test="${result != null}">
	${result.getFieldError("surname").getDefaultMessage()}
	</c:if>
		<input type="text" name="surname"><br>
		
		<label>age:</label><br>		
	<c:if test="${result != null}">
	${result.getFieldError("age").getDefaultMessage()}
	</c:if>
		<input type="text" name="age"><br>
		
		<label>Team name:</label><br>		
	<c:if test="${result != null}">
	${result.getFieldError("teamName").getDefaultMessage()}
	</c:if>
		<input type="text" name="teamName"><br>	
		
		
		
				<label>league</label><br>
		    		<select name="league">
			<c:forEach var="league" items="${leagues}">
				<option value="${league.name()}">${league.name()} </option>
			</c:forEach>
			</select><br>
		
		
  <input type="submit" value="Create Player">
  <br>
</form> 

</body>
</html>
