package repository.impl;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;







import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import repository.IMatchRepository;
import repository.IPlayerRepository;
import db.JPAUtils;
import domain.Match;
import domain.Player;
import domain.enums.League;

public class MatchRepositoryTest extends TestCase {

	IMatchRepository repomatch = new MatchRepository();
	IPlayerRepository repoplayer = new PlayerRepository();
	
	Match match1 = new Match();
	Match match2 = new Match();
	Match match3 = new Match();
	
	Player player1 = new Player();	
	Player player2 = new Player();
	Player player3 = new Player();
	
	
	
	@Override
	protected void setUp() throws Exception {
		JPAUtils.init();
	}
	
	@Test
	public void testAdd(){
		
		addMatch();
		int size = repomatch.getAll().size();
		assertEquals(3,size);	
	}
	
	@Test
	public void testDelete(){
		
		assertEquals(0,repomatch.getAll().size());


		
	}
	
	 @Test
	    public void testGet() {
	    	
	    	addMatch();
	    	Match fakeMatch = repomatch.get(match1.getId());
	    	assertEquals(fakeMatch, match1);


	    }

	    @Test
	    public void testGetAll() {
	    	
	    	addMatch();
	    	List<Match> matches = new ArrayList<Match>();
	    	matches.add(match1);
	    	matches.add(match2);
	    	matches.add(match3);
	    	assertEquals(matches, repomatch.getAll());
	    
	    }
	    

	
	
	@Test
	public void testGetByTeam(){

		addMatch();
		List<Match> f1Match = new ArrayList<Match>();
		List<Match> f2Match = repomatch.getByTeam("Lechia");
		f1Match.add(match2);
		f1Match.add(match3);
		assertEquals(f1Match,f2Match );

		
	}
	
	@Test
	public void testUpdate(){
		 addMatch();
		 Match fmatch1 = new Match();
		 
		 	fmatch1.setAwayTeam(player1);
		 	fmatch1.setAwayTeamGoals(2);
		 	fmatch1.setDate(null);
		 	fmatch1.setHomeTeam(player3);
		 	fmatch1.setHomeTeamGoals(1);
			
			assertNotSame(fmatch1, repomatch.get(1));
	} 	
		 	
	@Test
	public void testGetByHomeTeam(){

		addMatch();
		List<Match> f1Match = new ArrayList<Match>();
		List<Match> f2Match = repomatch.getByTeam("Lechia");
		f1Match.add(match2);
		assertEquals(f1Match,f2Match );


		
	}
	
	
	@Test
	public void testGetByAwayTeam(){

		addMatch();
		List<Match> f1Match = new ArrayList<Match>();
		List<Match> f2Match = repomatch.getByTeam("Lechia");
		f1Match.add(match3);
		assertEquals(f1Match,f2Match );


		
	}
	
	
	
	@After
	@Override
	protected void tearDown() throws Exception {
		repomatch.delete(match1);
		repomatch.delete(match2);
		repomatch.delete(match3);
		repoplayer.delete(player1);
		repoplayer.delete(player2);
		repoplayer.delete(player3);
	}
	
	@Before
	public void addMatch(){
	
		player1.setAge(1);
		player1.setLeague(League.Third);
		player1.setName("yoloo");
		player1.setNick("yolooo");
		player1.setPassword("yolooooo");
		player1.setSurname("yoloooooooo");
		player1.setTeamName("yoloooooooo");
		
		player2.setAge(43);
		player2.setLeague(League.Second);
		player2.setName("asf");
		player2.setNick("hdfs");
		player2.setPassword("yoasf");
		player2.setSurname("asfsfasoooo");
		player2.setTeamName("team");
		
		player3.setAge(11);
		player3.setLeague(League.Fourth);
		player3.setName("asfss");
		player3.setNick("yfasfo");
		player3.setPassword("yoassf");
		player3.setSurname("yolassffsfo");
		player3.setTeamName("Lechia");
		
		repoplayer.add(player1);
		repoplayer.add(player2);
		repoplayer.add(player3);
		
		match1.setHomeTeam(player2);
		match1.setAwayTeam(player1);
		match1.setHomeTeamGoals(2);
		match1.setAwayTeamGoals(4);

		match2.setHomeTeam(player3);
		match2.setAwayTeam(player2);
		match2.setHomeTeamGoals(1);
		match2.setAwayTeamGoals(0);
		
		match3.setHomeTeam(player1);
		match3.setAwayTeam(player3);
		match3.setHomeTeamGoals(3);
		match3.setAwayTeamGoals(2);
		
		repomatch.add(match1);
		repomatch.add(match2);
		repomatch.add(match3);
		
		
	
	}

}
