package repository.impl;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import repository.ITournamentRepository;
import db.JPAUtils;
import domain.Player;
import domain.Tournament;
import domain.enums.League;
import domain.enums.Platform;
import domain.enums.Status;

public class TournamentRepositoryTest extends TestCase{
	

ITournamentRepository repo = new TournamentRepository();

Player player1 = new Player();	
Player player2 = new Player();
Player player3 = new Player();
List<Player> prtcpnts = new ArrayList<Player>();

Tournament tnmt1 = new Tournament();
Tournament tnmt2 = new Tournament();
Tournament tnmt3 = new Tournament();

@Override
protected void setUp() throws Exception {
	JPAUtils.init();
}



@Test
public void testAdd(){

	addTnmt();
	int size = repo.getAll().size();
	assertEquals(3,size);


}


@Test
public void testGet() {
	
	addTnmt();
	Tournament fakeTmnt = repo.get(tnmt1.getId());
	assertEquals(fakeTmnt, tnmt1);


}

@Test
public void testGetAll() {
	
	addTnmt();
	List<Tournament> pom = new ArrayList<Tournament>();
	pom.add(tnmt1);
	pom.add(tnmt2);
	pom.add(tnmt3);
	assertEquals(pom, repo.getAll());

}


@Test
public void testDelete(){

    int	size = repo.getAll().size();
	assertEquals(0,size);
	
}

@Test
public void testUpdate(){
	 addTnmt();

	 Tournament pom1 = new Tournament();
	 
	 pom1.setDate(null);
	 pom1.setLeague(League.First);
	 pom1.setMaxNumberOfParticipants(10);
	 pom1.setName("Liga Mistrzow");
	 pom1.setParticipants(prtcpnts);
	 pom1.setPlatform(Platform.Playstation);
	 pom1.setStatus(Status.Finished);
	 pom1.setWinner(player2);
		
		assertNotSame(pom1, repo.get(1));
		
	
}

@Test
public void testGetByStatus(){
	addTnmt();
	List<Tournament> f1Tour = new ArrayList<Tournament>();
	List<Tournament> f2Tour = repo.getByStatus(Status.Entry);
	f1Tour.add(tnmt3);
	assertEquals(f1Tour,f2Tour);
	
}

@Test
public void testGetByPlatform(){
	addTnmt();
	List<Tournament> f1Tour = new ArrayList<Tournament>();
	List<Tournament> f2Tour = repo.getByPlatform(Platform.Xbox);
	f1Tour.add(tnmt2);
	assertEquals(f1Tour,f2Tour);
	
}

@Test
public void testGetByLeague(){

	List<Tournament> f1Tour = new ArrayList<Tournament>();
	List<Tournament> f2Tour = repo.getByLeague(League.First);
	f1Tour.add(tnmt1);
	assertEquals(f1Tour,f2Tour);
	

	 

	
}


@After
public void tearDown() throws Exception {
	repo.delete(tnmt1);
	repo.delete(tnmt2);
	repo.delete(tnmt3);
	prtcpnts.clear();
}

@Before
public void addTnmt(){
	player1.setAge(1);
	player1.setLeague(League.Third);
	player1.setName("yoloo");
	player1.setNick("yolooo");
	player1.setPassword("yolooooo");
	player1.setSurname("yoloooooooo");
	player1.setTeamName("yoloooooooo");
	
	player2.setAge(43);
	player2.setLeague(League.Second);
	player2.setName("asf");
	player2.setNick("hdfs");
	player2.setPassword("yoasf");
	player2.setSurname("asfsfasoooo");
	player2.setTeamName("team");
	
	player3.setAge(11);
	player3.setLeague(League.Fourth);
	player3.setName("asfss");
	player3.setNick("yfasfo");
	player3.setPassword("yoassf");
	player3.setSurname("yolassffsfo");
	player3.setTeamName("ooosasfsafooooo");
	
	prtcpnts.add(player1);
	prtcpnts.add(player2);
	prtcpnts.add(player3);
	
	tnmt1.setDate(null);
	tnmt1.setLeague(League.First);
	tnmt1.setMaxNumberOfParticipants(10);
	tnmt1.setName("Liga Mistrzow");
	tnmt1.setParticipants(prtcpnts);
	tnmt1.setPlatform(Platform.Playstation);
	tnmt1.setStatus(Status.Finished);
	tnmt1.setWinner(player1);
	
	tnmt2.setDate(null);
	tnmt2.setLeague(League.Second);
	tnmt2.setMaxNumberOfParticipants(3);
	tnmt2.setName("Puchar Europy");
	tnmt2.setParticipants(prtcpnts);
	tnmt2.setPlatform(Platform.Xbox);
	tnmt2.setStatus(Status.Finished);
	tnmt2.setWinner(player2);
	
	tnmt3.setDate(null);
	tnmt3.setLeague(League.Third);
	tnmt3.setMaxNumberOfParticipants(20);
	tnmt3.setName("Puchar Polski");
	tnmt3.setParticipants(prtcpnts);
	tnmt3.setPlatform(Platform.PC);
	tnmt3.setStatus(Status.Entry);
	tnmt3.setWinner(player3);
	
	
	repo.add(tnmt1);
	repo.add(tnmt2);
	repo.add(tnmt3);

}



}
