package repository.impl;

import java.util.ArrayList;
import java.util.List;





import org.junit.After;
import org.junit.Before;
import org.junit.Test;




import db.JPAUtils;
import repository.IPlayerRepository;
import domain.Player;
import domain.enums.League;
import junit.framework.TestCase;

public class PlayerRepositoryTest extends TestCase {

	
	IPlayerRepository repo = new PlayerRepository();
	
	Player player1 = new Player();	
	Player player2 = new Player();
	Player player3 = new Player();
	
	
	@Override
	protected void setUp() throws Exception {
		JPAUtils.init();
	}
	
	
	
	@Test
	public void testAdd(){

		addPlayer();
		int size = repo.getAll().size();
		assertEquals(3,size);


	}


    @Test
    public void testGet() {
    	
    	addPlayer();
    	Player fakePlayer = repo.get(player1.getId());
    	assertEquals(fakePlayer, player1);


    }

    @Test
    public void testGetAll() {
    	
    	addPlayer();
    	List<Player> players = new ArrayList<Player>();
    	players.add(player1);
    	players.add(player2);
    	players.add(player3);
    	assertEquals(players, repo.getAll());
    
    }
    
	
	@Test
	public void testDelete(){

		
		assertEquals(0,repo.getAll().size());
		
	}
	
	@Test
	public void testUpdate(){
		 addPlayer();
		 Player fplayer1 = new Player();
			fplayer1.setAge(11);
			fplayer1.setLeague(League.Third);
			fplayer1.setName("yoloo");
			fplayer1.setNick("yolooo");
			fplayer1.setPassword("yolooooo");
			fplayer1.setSurname("yoloooooooo");
			fplayer1.setTeamName("yoloooooooo");
			
			assertNotSame(fplayer1, repo.get(1));
			
		
	}
	
	@Test
	public void testGetByName(){
		addPlayer();
		List<Player> f1Player = new ArrayList<Player>();
		List<Player> f2Player = repo.getByName("yoloo");
		f1Player.add(player1);
		assertEquals(f1Player,f2Player );
		
	}
	
	@Test
	public void testGetByTeamName(){
		addPlayer();
		List<Player> f1Player = new ArrayList<Player>();
		List<Player> f2Player = repo.getByTeamName("team");
		f1Player.add(player2);
		assertEquals(f1Player,f2Player );
		
	}
	
	@Test
	public void testGetByLeague(){
		addPlayer();
		List<Player> f1Player = new ArrayList<Player>();
		List<Player> f2Player = repo.getByLeague(League.Fourth);
		f1Player.add(player3);
		assertEquals(f1Player,f2Player );
		

		 

		
	}
	
	
	@After
	@Override
	public void tearDown() throws Exception {
		repo.delete(player1);
		repo.delete(player2);
		repo.delete(player3);
		

	}
	
	@Before
	public void addPlayer(){
		player1.setAge(1);
		player1.setLeague(League.Third);
		player1.setName("yoloo");
		player1.setNick("yolooo");
		player1.setPassword("yolooooo");
		player1.setSurname("yoloooooooo");
		player1.setTeamName("yoloooooooo");
		
		player2.setAge(43);
		player2.setLeague(League.Second);
		player2.setName("asf");
		player2.setNick("hdfs");
		player2.setPassword("yoasf");
		player2.setSurname("asfsfasoooo");
		player2.setTeamName("team");
		
		player3.setAge(11);
		player3.setLeague(League.Fourth);
		player3.setName("asfss");
		player3.setNick("yfasfo");
		player3.setPassword("yoassf");
		player3.setSurname("yolassffsfo");
		player3.setTeamName("ooosasfsafooooo");
		
		repo.add(player1);
		repo.add(player2);
		repo.add(player3);
	
	}

	
	
}
